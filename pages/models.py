from django.db import models


class Student(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    age = models.IntegerField()
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Animal(models.Model):
    nume_animal = models.CharField(max_length=256)
    tip_animal = models.CharField(max_length=256)
    age = models.IntegerField()
    culoare = models.CharField(max_length=256)
    este_mamifer = models.BooleanField()
    creat_la = models.DateTimeField(auto_now_add=True)
    actualizat_la = models.DateTimeField(auto_now=True)
