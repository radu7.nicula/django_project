from django.urls import path

from pages import views

urlpatterns = [
    path('', views.index, name='pages'),
    path('blog_post_page/', views.blog_post_page, name='posts'),
    path('create_student/', views.StudentCreateView.as_view(), name='create_new_student'),
    path('list_of_students/', views.StudentsListView.as_view(), name='students_list'),
    path('create_animal/', views.AnimalCreateView.as_view(), name='create_new_animal'),
    path('list_of_animals/', views.AnimalListView.as_view(), name='animals_list'),
    path('update_student/<int:pk>', views.StudentUpdateView.as_view(), name='update-student'),
    path('delete_student/<int:pk>', views.StudentDeleteView.as_view(), name='delete-student'),
    path('detail_student/<int:pk>', views.StudentDetailView.as_view(), name='detail-student'),
    path('update_animal/<int:pk>', views.AnimalUpdateView.as_view(), name='update-animal'),
    path('delete_animal/<int:pk>', views.AnimalDeleteView.as_view(), name='delete-animal'),
    path('detail_animal/<int:pk>', views.AnimalDetailView.as_view(), name='detail-animal'),

]
