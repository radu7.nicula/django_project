import datetime

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from pages.models import Student, Animal

current_time = datetime.datetime.now()


def index(request):
    context = {
        'blog_entries': [
            {
                'title': 'hello world',
                'body': 'I have created my first template in Django',
            },
            {
                'title': 'Hi Radu',
                'body': 'How are you?',
            },
        ]

    }
    return render(request, "pages/index.html", context)


def blog_post_page(request):
    context = {
        'post_entries': [
            {
                'author_name': 'John',
                'content': 'Hello',
                'date': datetime.datetime.now(),
                'image': 'https://images.greelane.com/proxy?url=https%3A%2F%2Fwww.thoughtco.com%2Fthmb%2FpC5nDE3ssDPA'
                         '-lQoBMcfm192UXs%3D%2F1500x1000%2Ffilters%3Afill%28auto%2C1%29%2FSaharaDesert'
                         '-58c1a5603df78c353c3d525d.jpg&width=750',
                'current_date_time': datetime.datetime.now(),
            },
            {
                'author_name': 'Maria',
                'content': 'Salut',
                'date': datetime.datetime.now() - datetime.timedelta(days=30),
                'image': 'https://pics.filmaffinity.com/Creed-232228847-large.jpg',
                'current_date_time': datetime.datetime.now(),
            }
        ]
    }
    return render(request, 'pages/new_index.html', context)


class StudentCreateView(CreateView):
    template_name = 'pages/student_create.html'
    model = Student
    # fields = '__all__,'
    fields = ['first_name', 'last_name', 'age']
    success_url = reverse_lazy('create_new_student')


class StudentsListView(ListView):
    template_name = 'pages/student_list.html'
    model = Student
    context_object_name = 'all_students'


class StudentUpdateView(UpdateView):
    template_name = 'pages/update_student.html'
    model = Student
    context_object_name = 'all_students'
    fields = ['first_name', 'last_name', 'age']
    success_url = reverse_lazy('students_list')


class StudentDeleteView(DeleteView):
    template_name = 'pages/delete_student.html'
    model = Student
    context_object_name = 'student'
    success_url = reverse_lazy('students_list')


class StudentDetailView(DetailView):
    template_name = 'pages/detail_student.html'
    model = Student
    context_object_name = 'student'


class AnimalCreateView(CreateView):
    template_name = 'pages/animal_create.html'
    model = Animal
    fields = '__all__'
    # fields = ['nume_animal', 'tip_animal', 'age']
    success_url = reverse_lazy('create_new_animal')


class AnimalListView(ListView):
    template_name = 'pages/animal_list.html'
    model = Animal
    context_object_name = 'all_animals'


class AnimalUpdateView(UpdateView):
    template_name = 'pages/update_animal.html'
    model = Animal
    context_object_name = 'all_animals'
    fields = '__all__'
    success_url = reverse_lazy('animals_list')


class AnimalDeleteView(DeleteView):
    template_name = 'pages/delete_animal.html'
    model = Animal
    context_object_name = 'animal'
    success_url = reverse_lazy('animals_list')


class AnimalDetailView(DetailView):
    template_name = 'pages/detail_animal.html'
    model = Animal
    context_object_name = 'animal'
